# Wardrobify

Team:

* Janete Hays - Hats
* Ted O'Brien - Shoes

## Design

## Shoes microservice

Added in properties of manufacturer, model name, color, and bin (using the BinVO) for the Shoe model. Also created a BinVO model that had properties from the Bin model as well as an import href. Set up polling to update the BinVO from the wardrobify-api. Created a set of RESTful api's for the list view and detail view GET requests. Also created views for a POST, DELETE, and PUT request. Created a react component for viewing the list with the shoes as cards. Added a react component for submitting a new shoe. Added a delete button on the list page for each pair of shoes. Added a navlink to new shoes in the nav bar.

## Hats microservice

Fabric, Style, Color these will have Charfields
Url - pictureurl
LocationVO - foreignkey
For LocationVO I decided to use the closet name to know where hats will belong, also used  the unique href
