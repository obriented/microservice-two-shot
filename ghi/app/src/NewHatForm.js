import React from 'react';

class NewHatForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            fabric: '',
            style: '',
            color: '',
            image_url: '',
            location: '',
            locations: [],
        };
        this.handleFabricChange = this.handleFabricChange.bind(this);
        this.handleStyleChange = this.handleStyleChange.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this);
        this.handleImageChange = this.handleImageChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/locations';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json()
            this.setState({locations: data.locations});
        }
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.locations;
    
        const hatsUrl = 'http://localhost:8090/api/hats/';
        const fetchOptions = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        },
        };
        const hatResponse = await fetch(hatsUrl, fetchOptions);
        if (hatResponse.ok) {
        this.setState ({
            fabric: '',
            style: '',
            color: '',
            image_url: '',
            location: '',
        });
        }
    }
    
    handleFabricChange(event) {
        const value = event.target.value;
        this.setState({fabric: value})
    }
    
    handleStyleChange(event) {
        const value = event.target.value;
        this.setState({style: value})
    }
    
    handleColorChange(event) {
        const value = event.target.value;
        this.setState({color: value})
    }
    
    handleImageChange(event) {
        const value = event.target.value;
        this.setState({image_url: value})
    }
    
    handleLocationChange(event) {
        const value = event.target.value;
        this.setState({location: value})
    }

render () {
        return (
            <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new hat</h1>
                    <form onChange={this.handleSubmit} id="create-hat-from">
                        <div className="form-floating mb-3">
                            <input onChange={this.handleStyleChange} placeholder = "Style" required type = "text" name="style" id="style" className="form-control" value={this.state.style} />
                            <label htmlFor="color">Style</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleFabricChange} placeholder = "Fabric" required type = "text" name="fabric" id="fabric" className="form-control" value={this.state.fabric} />
                            <label htmlFor="fabric">Fabric</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleColorChange} placeholder = "Color" required type = "text" name="color" id="color" className="form-control" value={this.state.color} />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleImageChange} placeholder = "Image" required type = "url" name="image_url" id="image_url" className="form-control" value={this.state.image_url} />
                            <label htmlFor="image_url">Image</label>
                        </div>
                        <div className="form-floating mb-3">
                            <select onChange={this.handleLocationChange} required name="location" id="location" className="form-select" >
                            <option value ="">Choose a location</option>
                            {this.state.locations.map(locations => {
                                return (
                                    <option key={locations.id} value={locations.id}>
                                        {locations.closet_name}
                                    </option>
                                )
                            })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
            </div>
            </div>
            </div>
                    );
            }
        }

export default NewHatForm;