import React from 'react';
import { Link } from 'react-router-dom';

function ShoeColumn(props) {
  return (
    <div className="col">
      {props.list.map(shoe => {
        console.log(shoe);
        return (
          <div key={shoe.id} className="card mb-3 shadow">
            <img src={shoe.picture_url} className="card-img-top" />
            <div className="card-body">
              <h5 className="card-title">{shoe.model_name} by {shoe.manufacturer}</h5>
              <h6 className="card-subtitle mb-2 text-muted">
                {shoe.color}
              </h6>
              <p className="card-text">
                Located in {shoe.bin.closet_name} bin #{shoe.bin.bin_number}
              </p>
            </div>
            <div className="card-footer">
                <button onClick={() => props.handleDelete(shoe)} className='justify-content-center btn btn-danger'>Delete</button>
            </div>
          </div>
        );
      })}
    </div>
  );
}

class ShoesListCards extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      shoeColumns: [[], [], []],
    };
  }




  async componentDidMount() {
    const url = 'http://localhost:8080/api/shoes/';

    try {
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        console.log("1");
        console.log(data);
        console.log("2");
        console.log(data.shoes);

        // Create a list of for all the requests and
        // add all of the requests to it
        const requests = [];
        for (let shoe of data.shoes) {
          const detailUrl = `http://localhost:8080/api/shoes/${shoe.id}`;
          requests.push(fetch(detailUrl));
        }

        // Wait for all of the requests to finish
        // simultaneously
        const responses = await Promise.all(requests);

        // Set up the "columns" to put the conference
        // information into
        const shoeColumns = [[], [], []];

        // Loop over the conference detail responses and add
        // each to to the proper "column" if the response is
        // ok
        let i = 0;
        for (const shoeResponse of responses) {
          if (shoeResponse.ok) {
              const details = await shoeResponse.json();
              shoeColumns[i].push(details);
              console.log(shoeColumns[i])
              i = i + 1;
              if (i > 2) {
                i = 0;
              }
            } else {
              console.error(shoeResponse);
            }
        }
        console.log(shoeColumns);

        // Set the state to the new list of three lists of
        // conferences
        this.setState({shoeColumns: shoeColumns});
        this.setState({tempShoeColumns: shoeColumns})
        console.log(this.state);
        console.log(this.state.shoeColumns);
        console.log(this.state.shoeColumns[0]);
        console.log(this.state.shoeColumns[0][0])
      }
    } catch (e) {
      console.error(e);
    }
  }

  handleDelete = async (shoeToDelete) => {
    const locationUrl = `http://localhost:8080/api/shoes/${shoeToDelete.id}/`;
    const fetchConfig = {
      method: "delete",
      headers: {
        'Content-Type': 'application/json',
      }
    };
    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      // const deletedBoolean = await response.json();
      // this.setState ({hats: this.state.hats.filter(hat => hat.id != id)})
      let bridge = [
        [...this.state.shoeColumns[0].filter(shoe => shoe.id != shoeToDelete.id)],
        [...this.state.shoeColumns[1].filter(shoe => shoe.id != shoeToDelete.id)],
        [...this.state.shoeColumns[2].filter(shoe => shoe.id != shoeToDelete.id)],
      ];
      this.setState({shoeColumns: bridge});
    }
  };

  render() {
    return (
      <>
        <div className="px-4 py-5 my-5 mt-0 text-center bg-info">
          <img className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg" alt="" width="600" />
          <h1 className="display-5 fw-bold">Super sick kicks</h1>
          <div className="col-lg-6 mx-auto">
            <p className="lead mb-4">
              Checkout everything we have on hand at the moment.
            </p>
          </div>
        </div>
        <div className="container">
          <h1>Check it out</h1>
          <div className="row">
            {this.state.shoeColumns.map((shoeList, index) => {
              return (
                <ShoeColumn handleDelete={this.handleDelete} key={index} list={shoeList} />
              );
            })}
          </div>
        </div>
      </>
    );
  }
}

export default ShoesListCards;
