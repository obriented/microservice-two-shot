import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ListHats from './HatList';
import NewHatForm from './NewHatForm'
import ShoesForm from './ShoesForm';
import ShoesListCards from './ShoesListCards';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/hats" element={<ListHats/>} />
          <Route path="/hats/new" element={<NewHatForm />} />
          <Route path="shoes" element={<ShoesListCards />} />
          <Route path='shoes/new' element={<ShoesForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
